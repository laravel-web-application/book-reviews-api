# Book reviews API
### Things todo list:
1. Clone the REPO: `https://gitlab.com/laravel-web-application/book-reviews-api.git`
2. Go inside the folder: `cd book-reviews-api`
3. Run `cp .env.example .env`
4. Add database variables in .env
5. Run `composer install`
6. Run `php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"`
7. Run `php artisan jwt:secret`
8. Run `php artisan migrate`
9. Run `php artisan serve`
10. Run `php artisan route:list` to view all applicable routes and their methods

### Screen shot

Register New User

![Register New User](img/register.png "Register New User")

Login User

![Login User](img/login.png "Login User")

Add New Book

![Add New Book](img/add.png "Add New Book")

List All Books

![List All Books](img/list.png "List All Books")

Update Book

![Update Book](img/update.png "Update Book")

Add a rating Book

![Add a rating Book](img/rating.png "Add a rating Book")
